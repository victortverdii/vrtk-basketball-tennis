﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class CharacterTennisScript : MonoBehaviour {

	private Vector3 characterStartPoint;

	public VRTK_ControllerEvents LeftController; 
	public VRTK_ControllerEvents RightController;

	// Use this for initialization
	void Start ()
	{
		LeftController.ButtonTwoPressed += MenuButtonPressed;
		RightController.ButtonTwoPressed += MenuButtonPressed;

		characterStartPoint = new Vector3 (-1.0f, 0.0f, -1.35f);
	}
	
	// Update is called once per frame
	/*void Update ()
	{
		
	}*/

	private void MenuButtonPressed(object sender, ControllerInteractionEventArgs e)
	{

		gameObject.transform.position = characterStartPoint;
		Debug.Log ("Player position has been reset!");
	}
}
