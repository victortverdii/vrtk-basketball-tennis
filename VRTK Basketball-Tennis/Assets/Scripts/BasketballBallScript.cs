﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;

public class BasketballBallScript : MonoBehaviour {

	private Vector3 ballStartPoint;

	private int score;
	public Text scoreText;
	public GameObject player;
	public GameObject basketTrigger;

	public VRTK_ControllerEvents LeftController; 
	public VRTK_ControllerEvents RightController;

	// Use this for initialization
	void Start () {
		score = 0;
		ballStartPoint = new Vector3 (0, 1, 0.325f);

		LeftController.ButtonTwoPressed += MenuButtonPressed;
		RightController.ButtonTwoPressed += MenuButtonPressed;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (gameObject.transform.position.y <= -0.75)
		{
			BallReset ();
		}
	}

	private void BallReset()
	{
		gameObject.transform.position = ballStartPoint;
		gameObject.GetComponent<Rigidbody> ().isKinematic = true;
		gameObject.GetComponent<Rigidbody> ().isKinematic = false;
		Debug.Log (gameObject.name + " position has been reset!");
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "BasketTrigger")
		{
			score += 1;
			scoreText.text = score.ToString ();
		}
		else if (other.tag == "BasketArea")
		{
			basketTrigger.SetActive (false);
			Debug.Log ("Enter");
		}
	}

	private void MenuButtonPressed(object sender, ControllerInteractionEventArgs e)
	{
		BallReset ();
	}


	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "BasketArea")
		{
			basketTrigger.SetActive (true);
			Debug.Log ("Exit");
		}
	}
}
