﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;

public class TennisBallScript : MonoBehaviour {


	//1) Delaem cherez bounce.
	//2) Cherez masu
	//3) Cherez vrtk
	//4) libo peredavat addFOrce na shar pri soprikosnovenii s rokatkoy

	private Vector3 ballStartPoint;
	private int score;
	private float timeOnTable;

	public Text scoreText, highScoreText;
	public GameObject player/*, startBallCollider*/;

	public VRTK_ControllerEvents LeftController; 
	public VRTK_ControllerEvents RightController;


	// Use this for initialization
	void Start () {
		highScoreText.text = PlayerPrefs.GetInt ("High score").ToString ();
		score = 0;

		ballStartPoint = new Vector3 (0.0f, 2.0f, -1.0f);
		gameObject.transform.position = ballStartPoint;

		LeftController.ButtonTwoPressed += MenuButtonPressed;
		RightController.ButtonTwoPressed += MenuButtonPressed;
	}

	// Update is called once per frame
	void Update ()
	{
		if (gameObject.transform.position.y <= 0)
		{
			Restart ();
		}

//		else if (gameObject.transform.position.x != 0 && gameObject.transform.position.z != -0.3f)
//		{
//			startBallCollider.SetActive (false);
//		}
	}

	private void Restart()
	{
		if (score > PlayerPrefs.GetInt ("High score"))
		{
			PlayerPrefs.SetInt ("High score", score);
		}

		score = 0;
		scoreText.text = score.ToString ();
		highScoreText.text = PlayerPrefs.GetInt ("High score").ToString ();

		BallReset ();
	}

	private void BallReset()
	{
		gameObject.transform.position = ballStartPoint;
		gameObject.GetComponent<Rigidbody> ().isKinematic = true;
		gameObject.GetComponent<Rigidbody> ().isKinematic = false;
		Debug.Log (gameObject.name + " position has been reset!");

		//startBallCollider.SetActive (true);
	}

	private void MenuButtonPressed(object sender, ControllerInteractionEventArgs e)
	{
		Restart ();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "WallScoreTrigger")
		{
			score += 1;
			scoreText.text = score.ToString ();

			if (score > PlayerPrefs.GetInt ("High score"))
			{
				PlayerPrefs.SetInt ("High score", score);
				highScoreText.text = PlayerPrefs.GetInt ("High score").ToString ();
			}
		}
		else if (other.tag == "Walls" || other.tag == "Floor")
		{
			Restart ();
		}
	}

	//If ball is on table
	private void OnTriggerStay(Collider other)
	{
		if (other.tag == "TableCollider")
		{
			timeOnTable += 1;
			if (timeOnTable == 50)
			{
				Restart ();
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		timeOnTable = 0;
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Circle")
		{
			float thrust = 10.0f;
			GetComponent<Rigidbody> ().AddForce (transform.forward * thrust);
			Debug.Log ("RocketHit");
//			{
//				if (VRTK_ControllerReference.IsValid(controllerReference) && IsGrabbed())
//				{
//					collisionForce = VRTK_DeviceFinder.GetControllerVelocity(controllerReference).magnitude * impactMagnifier;
//					var hapticStrength = collisionForce / maxCollisionForce;
//					VRTK_ControllerHaptics.TriggerHapticPulse(controllerReference, hapticStrength, 0.5f, 0.01f);
//				}
//				else
//				{
//					collisionForce = collision.relativeVelocity.magnitude * impactMagnifier;
//				}
//			Vector3 velocity = other.gameObject.GetComponent<Rigidbody> ().velocity;
//			velocity.x *= 122;
//			velocity.y *= 122;
//			velocity.z *= 122;
//			gameObject.GetComponent<Rigidbody> ().AddRelativeForce (velocity.x, velocity.y, velocity.z, ForceMode.Impulse);
		}
	}
}
